﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        public static bool IsPrime(int n)
        {
            if (n == 2)  return true;
            if (n == 1) return false;
            for (int i = 2; i <= n / 2; i++)
            {
                if (n % i == 0)
                {
                    return false;
                }
            }
            return true;
        }

        static void Main(string[] args)
        {
            String s = Console.ReadLine();
            Console.WriteLine("Manoj");
            int[] a = new int[s.Length];
            for (int i = 0; i < s.Length; i++)
            {
                a[i] = s[i];
                if (i > 0)
                {
                    int n = (a[i] + a[i - 1]) / 2;
                    if (IsPrime(n))
                        n = n + 1;
                    Console.Write((char)n);
                }
            }
            Console.ReadKey();
        }
    }
}


